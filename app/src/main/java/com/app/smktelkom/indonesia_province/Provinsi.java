package com.app.smktelkom.indonesia_province;

/**
 * Created by SMK TELKOM on 05/01/2018.
 */

public class Provinsi {

    private String nama, tgl_berdiri, ibu_kota, situs_web, sejarah, kepadatan, luas, foto, logo;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTgl_berdiri() {
        return tgl_berdiri;
    }

    public void setTgl_berdiri(String tgl_berdiri) {
        this.tgl_berdiri = tgl_berdiri;
    }

    public String getIbu_kota() {
        return ibu_kota;
    }

    public void setIbu_kota(String ibu_kota) {
        this.ibu_kota = ibu_kota;
    }

    public String getSitus_web() {
        return situs_web;
    }

    public void setSitus_web(String situs_web) {
        this.situs_web = situs_web;
    }

    public String getSejarah() {
        return sejarah;
    }

    public void setSejarah(String sejarah) {
        this.sejarah = sejarah;
    }

    public String getKepadatan() {
        return kepadatan;
    }

    public void setKepadatan(String kepadatan) {
        this.kepadatan = kepadatan;
    }

    public String getLuas() {
        return luas;
    }

    public void setLuas(String luas) {
        this.luas = luas;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getLogo() { return logo; }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}