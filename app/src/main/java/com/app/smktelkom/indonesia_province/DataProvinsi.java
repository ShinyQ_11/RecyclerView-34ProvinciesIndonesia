package com.app.smktelkom.indonesia_province;

import java.util.ArrayList;

/**
 * Created by SMK TELKOM on 05/01/2018.
 */

public class DataProvinsi {

    public static String[][] data = new String[][]{
            {
                    "1. Provinsi Aceh",
                    "7 Desember 1959",
                    "Banda Aceh",
                    "www.acehprov.go.id",
                    "Aceh " +
                            "adalah sebuah provinsi di Indonesia. Aceh terletak di ujung utara pulau Sumatera dan merupakan " +
                            "provinsi paling barat di Indonesia. Ibu kotanya adalah Banda Aceh. Jumlah penduduk provinsi ini " +
                            "sekitar 4.500.000 jiwa.Aceh dianggap sebagai tempat dimulainya penyebaran Islam di Indonesia " +
                            "dan memainkan peran penting dalam penyebaran Islam di Asia Tenggara. Pada awal abad ke-17, " +
                            "Kesultanan Aceh adalah negara terkaya, terkuat, dan termakmur di kawasan Selat Malaka. Sejarah " +
                            "Aceh diwarnai oleh kebebasan politik dan penolakan keras terhadap kendali orang asing, termasuk " +
                            "bekas penjajah Belanda dan pemerintah Indonesia. Jika dibandingkan dengan dengan provinsi " +
                            "lainnya, Aceh adalah wilayah yang sangat konservatif (menjunjung tinggi nilai agama)." +
                            "Persentase penduduk Muslimnya adalah yang tertinggi di Indonesia dan mereka hidup sesuai " +
                            "syariah Islam.[10] Berbeda dengan kebanyakan provinsi lain di Indonesia, Aceh memiliki " +
                            "otonomi yang diatur tersendiri karena alasan sejarah.",
                    "4.494.410 jiwa",
                    "57.365,09 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=YHut4cmWS6jNSUHyJZyaDSa_WDUWqBGpe-ptWdS5C2ozl3cItQZSimvWzyH01ZsAChb1HLeTJ6omKlFdk74zZ68NvDAEWcLYATEQTLeyjOXpshNcCCS4EYw6io-9XO33vlzIEjtvvgnwI_TzADkwKGmJIY3aQEgG0Uvj6vuKN6pyUbxv3WPA6OXDVwbwJR8wNDs5QVv6hXHx8uC1ccNASfpVTqmTPVjVyENyNXNdXyMWMeGWolWZANlbbC5KcOh1ayqbw5fFVeS_5liVIYj-Y7rzsBlOITFD7vjKpAIkAVKg1Q",
                    "https://seeklogo.com/images/P/PANCACITA_ACEH-logo-300CE47472-seeklogo.com.png"
            },

            {
                    "2. Provinsi Bali",
                    "14 Agustus 1958",
                    "Denpasar",
                    "www.baliprov.go.id",
                    "Bali " +
                            "adalah sebuah provinsi di Indonesia. Ibu kota provinsi ini adalah Denpasar. Bali juga merupakan salah" +
                            " satu pulau di Kepulauan Nusa Tenggara. Di awal kemerdekaan Indonesia, pulau ini termasuk dalam " +
                            "Provinsi Sunda Kecil yang beribu kota di Singaraja, dan kini terbagi menjadi 3 provinsi: Bali, " +
                            "Nusa Tenggara Barat, dan Nusa Tenggara Timur.",
                    "3.890.757 jiwa",
                    "5.561,40 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=1XgLG2kOTqk1QLTaP_RRCYzSP-N7f9H3iD2YjXR1oVrAO2R7Zpx0J23uldklfe8Qbzc1l-qaHrGHWZQt6J4T4mgekukK3YqQeVuNYvaaav0CatOE_USMPGB9gTr2plK1wXQMAyOGyqcvra_MhX7xHDc71V6FU3HwyXMS1Q6OIMOcyd4eGJe5tOCMqcXk2Xh2IVh6ggWIBUDBwyv598scPTrIjFkfs4xBMZtCWH-xv7AmdnZgg9f1bdkB4ISf_9g1qmlFIkTMmTxabQHLVbXeaOjJGkoP8ASScmGxKecX53u5Rao",
                    "https://4.bp.blogspot.com/-ELlrLdH0frM/WSz4AjqIWaI/AAAAAAAAASY/EF5ayA5zXn05TXw53cRUVTJeh6lzUJDDwCLcB/s400/Lambang%2BDaerah%2BProvinsi%2BBali%2B2.png"
            },

            {
                    "3. Provinsi Banten",
                    "4 Oktober 2000",
                    "Serang",
                    "www.bantenprov.go.id",
                    "Banten" +
                            "adalah sebuah provinsi di Tatar Pasundan, serta wilayah paling barat di Pulau Jawa, Indonesia. " +
                            "Provinsi ini pernah menjadi bagian dari Provinsi Jawa Barat, namun menjadi wilayah pemekaran sejak " +
                            "tahun 2000, dengan keputusan Undang-Undang Nomor 23 Tahun 2000. Pusat pemerintahannya berada di Kota" +
                            "Serang.",
                    "10.632.166 jiwa",
                    "9.018,64 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=P8zggErpNyC4kFOo5W-8ruUm3C166jHgn97MiZtqxoxp1a8Oka0VIJAExz_dQ-1d2jvKwapS9rE1iemjae2_vaB6EY1kUN5cVVw47A2vbxeKCrDTkFmtJkjjhfK0RFFw0CCZEHvsWil2Rocmx2Cv3WwaHEduV5mNA7HjaXw2EYs_bM3zU-cJBzM0DnEK6jzjAtEC6zcsJoLzZ6P4T-aKCiJZMuuhKe4",
                    "https://upload.wikimedia.org/wikipedia/commons/1/1a/Banten_coa.png"
            },

            {
                    "4. Provinsi Bengkulu",
                    "18 November 1968",
                    "Bengkulu",
                    "www.bengkuluprov.go.id",
                    "Bengkulu" +
                            "adalah sebuah provinsi di Indonesia. Ibu kotanya berada di Kota Bengkulu. Provinsi ini terletak di" +
                            "bagian barat daya Pulau Sumatera.Di wilayah Bengkulu pernah berdiri kerajaan-kerajaan yang berdasarkan etnis " +
                            "seperti Kerajaan Sungai Serut, Kerajaan Selebar, Kerajaan Pat Petulai, Kerajaan Balai Buntar, Kerajaan " +
                            "Sungai Lemau, Kerajaan Sekiris, Kerajaan Gedung Agung, dan Kerajaan Marau Riang. Di bawah Kesultanan Banten, " +
                            "mereka menjadi vazal. Sebagian wilayah Bengkulu, juga pernah berada di bawah kekuasaan Kerajaan Inderapura " +
                            "semenjak abad ke-17.",
                    "1.715.518 jiwa",
                    "19.788,70 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=lQWq-8ufWE6l3qkv2EB1cV7xE7iZlZzro4NZ1oNUjX9n2PPmi9BrYIsJio64Lw-EgVC3972Elw_nYRzsQhtamkgk7glpPcQAvCRFKAutCK74UiZi7aEdY0O7PVG0_KCr6gn-sKW8id4-GGEc50z5NeYILe8icwYaHEVyeEOK2EvtTOb_MgJny0sqrMjrEzUTiJ2hHguqnv7-C08eMT9uKZ-4nM7pAhQ",
                    "https://upload.wikimedia.org/wikipedia/commons/0/03/Bengkulu_coa.png"
            },

            {
                    "5. Provinsi Gorontalo",
                    "22 Desember 2000",
                    "Gorontalo",
                    "www.gorontaloprov.go.id",
                    "Gorontalo " +
                            "adalah sebuah Provinsi di Indonesia yang lahir pada tanggal 5 Desember 2000. Seiring dengan " +
                            "munculnya pemekaran wilayah yang berkenaan dengan Otonomi Daerah di Era Reformasi, provinsi ini " +
                            "kemudian dibentuk berdasarkan Undang-Undang Nomor 38 Tahun 2000, tertanggal 22 Desember dan menjadi " +
                            "Provinsi ke-32 di Indonesia.",
                    "1.040.164 jiwa",
                    "11.967,64 km\u00b2",
                    "http://cdn2.tstatic.net/wartakota/foto/bank/images/peta-prov-gorontalo_20170715_211334.jpg",
                    "https://upload.wikimedia.org/wikipedia/commons/3/35/Lambang_propinsi_gorontalo.jpg"
            },

            {
                    "6. Provinsi Jakarta",
                    "28 Agustus 1961",
                    "Jakarta",
                    "www.jakarta.go.id",
                    "Daerah Khusus Ibukota Jakarta" +
                            " (DKI Jakarta) adalah ibu kota negara dan kota terbesar di Indonesia. Jakarta " +
                            "merupakan satu-satunya kota di Indonesia yang memiliki status setingkat provinsi. Jakarta terletak " +
                            "di pesisir bagian barat laut Pulau Jawa. Dahulu pernah dikenal dengan beberapa nama di antaranya Sunda" +
                            "Kelapa, Jayakarta, dan Batavia. Di dunia internasional Jakarta juga mempunyai julukan J-Town, atau " +
                            "lebih populer lagi The Big Durian karena dianggap kota yang sebanding New York City (Big Apple) di " +
                            "Indonesia",
                    "9.607.787 jiwa",
                    "740,28 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=sNOnvUKUa4NC8IeOr_yQkLvZs02maXiSDh0hrJpHqp3ey5jeGisTiepf5-RwjS4WVYYMQhN5RsUx8P06vUkzUE6urlseEYT6bMzi_CMXef6Zl7NQH7ygwreEB0r_zMxB5dmEn2DHvWIIbSYbnQq860jKaz1spN3L97fx96vJvI-lB10Tgra94tc9mY06veQeLnpN3ugFa1yZMxVf8fj0SX2RfZSoJA",
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Coat_of_arms_of_Jakarta.svg/425px-Coat_of_arms_of_Jakarta.svg.png"
            },

            {
                    "7. Provinsi Jawa Barat",
                    "4 Juli 1950",
                    "Bandung",
                    "www.jabarprov.go.id",
                    "Jawa Barat " +
                            "berada di bagian barat Pulau Jawa. Wilayahnya berbatasan dengan Laut Jawa di utara, Jawa Tengah " +
                            "di timur, Samudera Hindia di selatan, serta Banten dan DKI Jakarta di barat. Jawa Barat selama lebih " +
                            "dari tiga dekade telah mengalami perkembangan ekonomi yang pesat. Saat ini peningkatan ekonomi " +
                            "modern ditandai dengan peningkatan pada sektor manufaktur dan jasa. Disamping perkembangan sosial " +
                            "dan infrastruktur, sektor manufaktur terhitung terbesar dalam memberikan kontribusinya melalui " +
                            "investasi, hampir tigaperempat dari industri-industri manufaktur non minyak berpusat di sekitar Jawa " +
                            "Barat.",
                    "43.053.732 jiwa",
                    "740,28 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=nKru0Rsz54hWOHnVB8cDSv7sc0gf_7fVhv2zE8r9kqRMBXKAyZJ1YdZ96LHWpmka2fT9PYTYbj9GrMQRAs9wCpcOuM52V9A4IMx5qoyjgOTBsdZ92-Kgsv7WXm_9ha4M-3iQmmiwrga8AhIUxEcQB09KQLGyhzhK5UdmsteakbHYStwYxA887Pzhop6DpM9WG1SpA6BShPNvhXffZUR7TZ9fbNEeeoA",
                    "https://upload.wikimedia.org/wikipedia/commons/0/07/West_Java_coa.png"
            },

            {
                    "8. Provinsi Jawa Tengah",
                    "4 Juli 1950",
                    "Semarang",
                    "www.jatengprov.go.id",
                    "Jawa Tengah " +
                            "adalah sebuah provinsi Indonesia yang terletak di bagian tengah Pulau Jawa. Ibu kotanya adalah " +
                            "Semarang. Provinsi ini berbatasan dengan Provinsi Jawa Barat di sebelah barat, Samudra Hindia dan " +
                            "Daerah Istimewa Yogyakarta di sebelah selatan, Jawa Timur di sebelah timur, dan Laut Jawa di sebelah " +
                            "utara.",
                    "32.382.657 jiwa",
                    "33.987,45 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=gYSd89B54Ejm-odp5mVNJQbnyQlya4_PkBvpWrMNc_6u6t1urXQqR7qGPnXa8q0kz6eqHd8x4nd_2hyquCR4-VdFsALni8j7GIv4sub3qiMP_FJsF700lcHMokwZitkUaPiZJpccdwMIiQzb9ssUaTp0W0bZCrx5T7abjU2bmJih9B3tGRCNd2HFS_Cjx4iKaDmbRE_xsgkRCOKxkPtM3JRhFm02PII",
                    "http://4.bp.blogspot.com/-v7NQHsI5dsc/VNoAXuDYQxI/AAAAAAAAAX8/GohrLbdf3FE/s1600/logo%2Bjawa%2Btengah.png"
            },

            {
                    "9. Provinsi Jawa Timur",
                    "4 Juli 1950",
                    "Surabaya",
                    "www.jatimprov.go.id",
                    "Jawa Timur " +
                            "adalah sebuah provinsi di bagian timur Pulau Jawa, Indonesia. Ibu kotanya terletak di " +
                            "Surabaya. Luas wilayahnya 47.922 km², dan jumlah penduduknya 42.030.633 jiwa (sensus 2015). " +
                            "Jawa Timur memiliki wilayah terluas di antara 6 provinsi di Pulau Jawa, dan memiliki jumlah penduduk " +
                            "terbanyak kedua di Indonesia setelah Jawa Barat.",
                    "1.715.518 jiwa",
                    "19.788,70 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=HXR0en-bjnSoSWbD7rOblFxVqJ4i4f6PH4UfwrzAhH2A0zUcxq_BKqLNYODXzsuf99Y31yl33xIEZMOE-czvDd14p944F4zSE7HxPCZctaPVkueO_-5U_weBe3SFfo2QEdgO9FYuRCLVgpWV1NT7IYC_8nH4GaIWKd-o4c3w_g5S5RwTCmvReB5_h3GIUUVuNHcSr_Fe7x_o_z97U8rMmui_TpPGvA",
                    "https://i2.wp.com/www.logokabupaten.com/wp-content/uploads/2015/02/Logo-Provinsi-Jawa-Timur.jpg?ssl=1"
            },

            {
                    "10. Provinsi Kalimantan Barat",
                    "7 Desember 1956",
                    "Pontianak",
                    "www.kalbar.go.id",
                    "Kalimantan Barat " +
                            "adalah sebuah provinsi di Indonesia yang terletak di Pulau Kalimantan dengan ibu kota Provinsi Kota Pontianak." +
                            "Luas wilayah Provinsi Kalimantan Barat adalah 146.807 km² (7,53% luas Indonesia). Merupakan provinsi terluas " +
                            "keempat setelah Papua, Kalimantan Timur dan Kalimantan Tengah. Daerah Kalimantan Barat termasuk salah satu daerah" +
                            "yang dapat dijuluki provinsi  Seribu Sungai. Julukan ini selaras dengan kondisi geografis yang mempunyai ratusan " +
                            "sungai besar dan kecil yang di antaranya dapat dan sering dilayari. Beberapa sungai besar sampai saat ini masih " +
                            "merupakan urat nadi dan jalur utama untuk angkutan daerah pedalaman, walaupun prasarana jalan darat telah dapat " +
                            "menjangkau sebagian besar kecamatan",
                    "4.395.983 jiwa",
                    "115.114,32 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=hbUePVN-cO_jY8ViGdpQuK-P8qKd18hSwj5kpaUews8fya_SzRdn28HjR7wxgWqa2AvQeqemjdLDOhjJet36Dh-ebJnlGBOQaRRYvxJcUZmC24B2xz5mGQyeMfDYozV2PS3YPMQSB6CoVD6ihR2IH1fg5NXTStEx-p2T3j1BgbDFDhxWPvyTyRt9PKzOOyy0_B57CY45hselkLeFEJEwM3zWCr-4cRw",
                    "https://lambanglogo.files.wordpress.com/2011/09/logo-prop-kalbar.png"
            },

            {
                    "11. Provinsi Kalimantan Selatan",
                    "7 Desember 1956",
                    "Banjarmasin",
                    "www.kalselprov.go.id",
                    "Kalimantan Selatan " +
                            "adalah salah satu provinsi di Indonesia yang terletak di pulau Kalimantan. Ibu kotanya adalah Banjarmasin. " +
                            "Provinsi Kalimantan Selatan memiliki luas 37.530,52 km² dengan populasi hampir 3,7 juta jiwa Provinsi ini " +
                            "mempunyai 11 kabupaten dan 2 kota. DPRD Kalimantan Selatan dengan surat keputusan No. 2 Tahun 1989 tanggal " +
                            "31 Mei 1989 menetapkan 14 Agustus 1950 sebagai Hari Jadi Provinsi Kalimantan Selatan. Tanggal 14 Agustus 1950 " +
                            "melalui Peraturan Pemerintah RIS No. 21 Tahun 1950, merupakan tanggal dibentuknya provinsi Kalimantan, setelah " +
                            "pembubaran Republik Indonesia Serikat (RIS), dengan gubernur Dokter Moerjani. Secara historis wilayah Kalimantan " +
                            "Selatan mula-mula dibentuk merupakan wilayah Karesidenan Kalimantan Selatan di dalam Propinsi Kalimantan itu " +
                            "sendiri.",
                    "3.626.616 jiwa",
                    "36.805,34 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=KDnDALq0goEG6fDDHLjy8641Z5FGl0C-htrwo7oq-9gpdwUIoffcHHiIZSYH6RNPR0deYV3o2xRp389eao2_uP_lh9KUb3QQ8LK_K9iu6wMoIUbPOjLnVYeUbwcQ8UVRVrs1M5514Mys2bU9M11xCuSQNmY6iMbSy65GztI--J7F40GZTMuIspzCJD9OIrN8ZhQZWT0gWGwU9VDRxrZlfBlNIaTb2epOvTqmcAlOrFL0jjrBjv6QBSzYlV_UW1l6uIGQTPA4n9ObBTCCzuSPdIz40gvR2m5h8xuZbalvEc4vxIA",
                    "http://bpmpd.kalselprov.go.id/wp-content/uploads/2017/01/LOGO-PROVINSI-KALIMANTAN-SELATAN.png"
            },

            {
                    "12. Provinsi Kalimantan Tengah",
                    "2 Juli 1958",
                    "Palangkaraya",
                    "www.kalteng.go.id",
                    "Kalimantan Tengah " +
                            "adalah salah satu provinsi di Indonesia yang terletak di Pulau Kalimantan. Ibukotanya adalah Kota Palangka Raya." +
                            "Kalimantan Tengah memiliki luas 157.983 km². Berdasarkan sensus tahun 2010, provinsi ini memiliki populasi " +
                            "2.202.599 jiwa, yang terdiri atas 1.147.878 laki-laki dan 1.054.721 perempuan. Sensus penduduk 2015, jumlah " +
                            "penduduk Kalimantan Tengah bertambah menjadi 2.680.680 jiwa. Kalteng mempunyai 13 kabupaten dan 1 kota.",
                    "2.212.089 jiwa",
                    "153.564,50 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=BY4dniUX33Q-hn4RSsWtsCYbhtRc-mehqtFX2jWtk8hppE7CCHi4hF9__dTkf_uf07taSvu_52wAJBgB3qGjlJ08tYZJm9YmZM_gsmp5Lqm58NIIlLmFA-ZLulwXa_0jm4gGQ9x23C0is07j4JNwO_7679VvmEmtBzuu8ScBuR0Mi3pHAQ0_e6VAM1LKfbH_ownngnmTVJ6QgQEEG9--hKSNk9Xro1hKnHCan6Qh7IefUHPshNRRu3sWGQqSnd4QAJLKTXdVDciOggMf5-NZsV-gFKKhdKx8peGdmXz1iAsG8ls",
                    "https://upload.wikimedia.org/wikipedia/commons/e/eb/Coat_of_arms_of_Central_Kalimantan.png"
            },

            {
                    "13. Provinsi Kalimantan Timur",
                    "2 Juli 1958",
                    "Samarinda",
                    "www.kaltimprov.go.id",
                    "Kalimantan Timur " +
                            "dalah sebuah provinsi Indonesia di Pulau Kalimantan bagian ujung timur yang berbatasan dengan Malaysia, " +
                            "Kalimantan Utara, Kalimantan Tengah, Kalimantan Selatan, Kalimantan Barat, dan Sulawesi. Luas total Kaltim " +
                            "adalah 129.066,64 km² dan populasi sebesar 3.6 juta. Kaltim merupakan wilayah dengan kepadatan penduduk terendah" +
                            " keempat di nusantara. Ibukotanya adalah Samarinda.Kalimantan Timur sebelum mekar menjadi Kalimantan Utara " +
                            "merupakan provinsi terluas kedua di Indonesia setelah Papua, dengan luas 194.489 km persegi yang hampir sama " +
                            "dengan Pulau Jawa atau sekitar 6,8% dari total luas wilayah Indonesia.",
                    "2.212.089 jiwa",
                    "194.849,08 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=IYGaO8SJgz2u2wNd3Q1WidKlKL4rkXtLORweK2hnEp9n7l6qcuUD3baRwQWDA0PzkHjir0AW5znTxdZJqfjF8r47EpjNq-6ZwRfDABzMGElM-O5mLtxOsVjMFqXfycGje7p0YBIaYVlUTUuxJdDzdk2e2XXi7d4IVYNfLjUtRcvaTgFjlKuZM8xU0-QWTeqHDzgmeHdWmsAZLRNx2nBuA9ZecYuyAGg",
                    "https://4.bp.blogspot.com/-JggfSX8-Dj0/VNtHewi9DJI/AAAAAAAAAbQ/eKYDT2UQWzQ/s1600/logo+kalimantan+timur.png"
            },

            {
                    "14 Provinsi Kalimantan Utara",
                    "2 Juli 1958",
                    "Tanjung Selor",
                    "www.kaltaraprov.go.id",
                    "Kalimantan Utara " +
                            "adalah sebuah provinsi di Indonesia yang terletak di bagian utara Pulau Kalimantan. Provinsi ini berbatasan " +
                            "langsung dengan negara tetangga, yaitu Negara Bagian Sabah dan Serawak Saat ini, Kalimantan Utara merupakan " +
                            "provinsi termuda Indonesia, resmi disahkan menjadi provinsi dalam rapat paripurna DPR pada tanggal 25 Oktober " +
                            "2012 berdasarkan Undang-undang Nomor 20 Tahun 2012.Kementerian Dalam Negeri menetapkan 11 daerah otonomi baru" +
                            " yang terdiri atas satu provinsi dan 10 kabupaten, termasuk Kaltara pada hari Senin, 22 April 2013. Bersama " +
                            "dengan penetapan itu, Menteri Dalam Negeri Gamawan Fauzi melantik kepala daerah masing-masing, termasuk pejabat " +
                            "Gubernur Kaltara yakni Irianto Lambrie. Infrastruktur pemerintahan Kalimantan Utara masih dalam proses persiapan " +
                            "yang direncanakan akan berlangsung paling lama dalam 1 tahun Pada tanggal 22 April 2015, Menteri Dalam Negeri" +
                            "Tjahjo Kumolo melantik Triyono Budi Sasongko sebagai Pejabat Gubernur Kaltara menggantikan Irianto Lambrie yang " +
                            "telah menjabat selama 2 periode masa jabatan Pj. Gubernur Kaltara.",
                    "622.350 jiwa",
                    "71.176,72 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=u8uRszzvO6O3UoyWKJPKl7MgVie0MlslX_YxvlS1dzAe5z-Tf5Nb2tCYSPeT9VLQdNs2YbLEK8mlCZcRyrbAVnNaXbuLDGoggIhOM4LGfKwTQjEn-zGXAh4qUSmIhPAOgp9RDwNVDPs7MY-sTZ4PYwEFsyJ0Xe2_K8j_Pi4Ym_YQXdegQOEydabTUEVnUfCadA4fk2YvEerpT5V1vLyaIxL23ouu_W8",
                    "https://upload.wikimedia.org/wikipedia/commons/3/33/Emblem_of_North_Kalimantan.png"
            },

            {
                    "15. Provinsi Kepulauan Bangka Belitung",
                    "2 Juli 1958",
                    "Pangkal Pinang",
                    " www.babelprov.go.id",
                    "Bangka Belitung " +
                            "adalah salah satu provinsi di Indonesia yang terletak di Pulau Kalimantan. Ibukotanya adalah Kota Palangka Raya." +
                            "Kalimantan Tengah memiliki luas 157.983 km². Berdasarkan sensus tahun 2010, provinsi ini memiliki populasi " +
                            "2.202.599 jiwa, yang terdiri atas 1.147.878 laki-laki dan 1.054.721 perempuan. Sensus penduduk 2015, jumlah " +
                            "penduduk Kalimantan Tengah bertambah menjadi 2.680.680 jiwa. Kalteng mempunyai 13 kabupaten dan 1 kota.",
                    "1.043.456 jiwa",
                    "16.424,14 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=Jk0EkluEdYigajXomAohROY9CaDK3LbpJS8VfnNsfxlOK_Ayvbm8_Oazx6uX40NkCQhkEI9Nn9zy4II2L38a9iGcO_ixw5Q22yLdLicvc-Q6ObbLKv-Ft0r5XRJUbjT1Jyji2GnYCHkGtyNnxq5m35w9mIQfa6CH8s4JmJmKJgj2JVGJwEdeAlYwhe0gTMUYAJQq0JelnjIdn2Cht4ggigTw5AJdQN4",
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Coat_of_arms_of_Bangka_Belitung_Islands.svg/1200px-Coat_of_arms_of_Bangka_Belitung_Islands.svg.png"
            },

            {

            "16. Provinsi Riau",
            "9 Agustus 1957",
            "Pekanbaru",
            "www.riauprov.go.id",
            "Riau " +
                    "adalah sebuah provinsi di Indonesia yang terletak di bagian tengah pulau Sumatera. Provinsi ini terletak di bagian " +
                    "tengah pantai timur Pulau Sumatera, yaitu di sepanjang pesisir Selat Melaka. Hingga tahun 2004, provinsi ini juga meliputi" +
                    "Kepulauan Riau, sekelompok besar pulau-pulau kecil (pulau-pulau utamanya antara lain Pulau Batam dan Pulau Bintan) yang" +
                    "terletak di sebelah timur Sumatera dan sebelah selatan Singapura. Kepulauan ini dimekarkan menjadi provinsi tersendiri " +
                    "pada Juli 2004. Ibu kota dan kota terbesar Riau adalah Pekanbaru. Kota besar lainnya antara lain Dumai, Selatpanjang, " +
                    "Bagansiapiapi, Bengkalis, Bangkinang, Tembilahan, dan Rengat. Riau saat ini merupakan salah satu provinsi terkaya di " +
                    "Indonesia, dan sumber dayanya didominasi oleh sumber alam, terutama minyak bumi, gas alam, karet, kelapa sawit dan " +
                    "perkebunan serat. Tetapi, penebangan hutan yang merajalela telah mengurangi luas hutan secara signifikan, dari 78% pada " +
                    "1982 menjadi hanya 33% pada 2005.Rata-rata 160,000 hektare hutan habis ditebang setiap tahun, meninggalkan 22%, " +
                    "atau 2,45 juta hektare pada tahun 2009. Deforestasi dengan tujuan pembukaan kebun-kebun kelapa sawit dan produksi " +
                    "kertas telah menyebabkan kabut asap yang sangat mengganggu di provinsi ini selama bertahun-tahun, dan menjalar ke " +
                    "negara-negara tetangga seperti Malaysia dan Singapura.",
            "1.274.848 jiwa",
            "8.084,01 km\u00b2",
            "https://www.google.co.id/maps/vt/data=ayQIW2tjCKYQtkkj3E2dsByY3pMNfP8eJCkLZFiwb_SQkDmV8I6zt-8SlCOP6a4_CSUuhWpiYSoMxndYdE85o_H0zSRPkOMogfJPWU_lT5Z6qB2N5Yu7PMJ_ZZlXP1tBVkFocYa3xfY_wmBm8Qh9kFwm-cNgbByLvJ69B5EvYFZOzctDWlp6y0KhDMS43uYzLDQ5QX6_pJCFz2zMZxpc-Z5XobpwD3P5Z8uSM-N_M6OeuMG3LEVbM2fgGI5YFabhW0kjlfKl3-zDKLSZJB5l8eij0-n3Raocp9YcMh4Y--jlty4",
            "http://jdih.riau.go.id//assets/images/logo/pemda.png"

             },

            {
                    "17. Provinsi Kepualauan Riau",
                    "25 Oktober 2002",
                    "Tanjung Pinang",
                    "www.kepriprov.go.id",
                    "Kepulauan Riau " +
                            "adalah sebuah provinsi di Indonesia. Provinsi Kepulauan Riau berbatasan dengan Vietnam dan Kamboja di sebelah " +
                            "utara; Malaysia dan provinsi Kalimantan Barat di timur; provinsi Kepulauan Bangka Belitung dan Jambi di selatan; " +
                            "Negara Singapura, Malaysia dan provinsi Riau di sebelah barat. Provinsi ini termasuk provinsi kepulauan di " +
                            "Indonesia. Secara keseluruhan wilayah Kepulauan Riau terdiri dari 5 kabupaten, dan 2 kota, 52 kecamatan serta " +
                            "299 kelurahan/desa dengan jumlah 2.408 pulau besar, dan kecil yang 30% belum bernama, dan berpenduduk. Adapun luas" +
                            " wilayahnya sebesar 8.201,72 km², sekitar 95% merupakan lautan, dan hanya sekitar 5% daratan.",
                    "1.274.848 jiwa",
                    "8.084,01 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=UX-RHaBp4dnA392LyzF35CtVT3NveWDef9hMw4gVaWpGOierU7krnwZhxVTs2EmB1wvmn_2ivEXROFgVxM9OHmv1zqOfksmpaSeTV3MtfhVZNe6dzUERGWJDGuAW-N6k_7Shnh4T_6L4hTkITatEmgQWttyFCXXucD_yjp_tmDa3bJJHmagXpe7qGDapLSAfEBs3z25hXfw4kk2h5CDmnYMY4PnhXPmhgWvM2WTMQ3H5XOjCndhbbGbBGTCBNvjJueuIvHCKnF5fMmCkFBVN9hdeqVn0E3NwhinkjERMWcVin98",
                    "https://upload.wikimedia.org/wikipedia/commons/9/9b/Riau_Islands_COA.png"
            },

            {
                    "18. Provinsi Lampung",
                    "18 Maret 1964",
                    "Bandar Lampung",
                    "www.lampungprov.go.id",
                    "Lampung " +
                            "adalah sebuah provinsi paling selatan di Pulau Sumatera, Indonesia, Ibukotanya terletak di Bandar Lampung. " +
                            "Provinsi ini memilki 3 Kota dan 15 Kabupaten. Kota yang dimaksud adalah Kota Bandar Lampung , Kota Kotabumi dan " +
                            "Kota Metro. Disebelah utara berbatasan dengan Bengkulu dan Sumatera Selatan.Provinsi Lampung memiliki Pelabuhan" +
                            "utama bernama Pelabuhan Panjang dan Pelabuhan Bakauheni serta pelabuhan nelayan seperti Pasar Ikan (Telukbetung)," +
                            "Tarahan, dan Kalianda di Teluk Lampung. Bandar Udara utama adalah Radin Inten II, yaitu nama baru dari Branti, " +
                            "28 Km dari Ibukota melalui jalan negara menuju Kotabumi, dan tiga Bandar Udara perintis yaitu : Bandar Udara " +
                            "Mohammad Taufik Kiemas di Krui, Pesisir Barat, Bandar Udara Gatot Soebroto di Kabupaten Way Kanan dan Lapangan " +
                            "terbang AURI terdapat di Menggala yang bernama Astra Ksetra.",
                    "7.608.405 jiwa",
                    "35.376 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=npWWwxNxFyF4TkX9a7x5RsKPJHiKSv5WCRYNGGPPGv2bXeebVP_gFJ58K8QFUFTeokMhev2QfDw39I5p6fzJEtZlN7EFZGN18jCERrza4N6LIr99lEcARF5bD60i792an_wttTpZlNqbg3cAs0thx_D3y7ITxhTXnHEcNoFMy74tD7HSJyr6aYsLOIHXn74UCjSEdgWUrDWEhuV13kpsLjf61BGMbNGwMzA6xu5ZxDWP6rE3LcPyefL0g-pzwkSgoZhKcdnVTj7nAaInXeMjg8R0dukHuaIwdbVV0sD_dvFR0sI",
                    "https://3.bp.blogspot.com/-l0AqwII5pqY/VxCfnQs3SRI/AAAAAAAAPMw/1yYHV7Cd1rosWs6jf6wWhfekBJ-lQ0Z5QCLcB/s1600/Lampung_logo.png"
            },

            {
                    "19. Provinsi Maluku",
                    "17 Juni 1958",
                    "Ambon",
                    "www.malukuprov.go.id",
                    "Maluku " +
                            "adalah sebuah provinsi yang meliputi bagian selatan Kepulauan Maluku, Indonesia. Lintasan sejarah Maluku " +
                            "telah dimulai sejak zaman kerajaan-kerajaan besar di Timur Tengah seperti kerajaan Mesir yang dipimpin Firaun. " +
                            "Bukti bahwa sejarah Maluku adalah yang tertua di Indonesia adalah catatan tablet tanah liat yang ditemukan di " +
                            "Persia, Mesopotamia, dan Mesir menyebutkan adanya negeri dari timur yang sangat kaya, merupakan tanah surga, " +
                            "dengan hasil alam berupa cengkeh, emas dan mutiara, daerah itu tak lain dan tak bukan adalah tanah Maluku yang " +
                            "memang merupakan sentra penghasil Pala, Fuli, Cengkeh dan Mutiara. Pala dan Fuli dengan mudah didapat dari Banda " +
                            "Kepulauan, Cengkeh dengan mudah ditemui di negeri-negeri di Ambon, Pulau-Pulau Lease (Saparua, Haruku & Nusa " +
                            "laut) dan Nusa Ina serta Mutiara dihasilkan dalam jumlah yang cukup besar di Kota Dobo, Kepulauan Aru.",
                    "1.568.292 jiwa",
                    "49.350,42 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=Jk0EkluEdYigajXomAohROY9CaDK3LbpJS8VfnNsfxlOK_Ayvbm8_Oazx6uX40NkCQhkEI9Nn9zy4II2L38a9iGcO_ixw5Q22yLdLicvc-Q6ObbLKv-Ft0r5XRJUbjT1Jyji2GnYCHkGtyNnxq5m35w9mIQfa6CH8s4JmJmKJgj2JVGJwEdeAlYwhe0gTMUYAJQq0JelnjIdn2Cht4ggigTw5AJdQN4",
                    "https://upload.wikimedia.org/wikipedia/commons/9/94/Maluku_coa.png"
            },

            {
                    "20. Provinsi Maluku Utara",
                    "4 Oktober 1999",
                    "Sofifi",
                    "www.malutprov.go.id",
                    "Maluku Utara " +
                            "adalah salah satu provinsi di Indonesia. Maluku Utara resmi terbentuk pada tanggal 4 Oktober 1999, melalui UU RI " +
                            "Nomor 46 Tahun 1999 dan UU RI Nomor 6 Tahun 2003. Sebelum resmi menjadi sebuah provinsi, Maluku Utara merupakan" +
                            " bagian dari Provinsi Maluku, yaitu Kabupaten Maluku Utara dan Kabupaten Halmahera Tengah. Pada awal pendiriannya," +
                            " Provinsi Maluku Utara beribukota di Ternate yang berlokasi di kaki Gunung Gamalama, selama 11 tahun. Tepatnya" +
                            " sampai dengan 4 Agustus 2010, setelah 11 tahun masa transisi dan persiapan infrastruktur, ibukota Provinsi" +
                            " Maluku Utara dipindahkan ke Kota Sofifi yang terletak di Pulau Halmahera yang merupakan pulau terbesarnya.",
                    "1.135.478 jiwa",
                    "42.959,99 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=AaxLC0rgxgiv1j6XcyJIn5baJaFv0nHOivQjUmaaD6toZH3iqsuwPsfts8akQWliNvlCSiNhnpw3YoMLfWfnTNZBb7DU4-gXIOGWhMfyge2PccG4AZgk6Djq2vArVbS9Rlg5HETInwlxoeh-LwwApS9u40XAfofRyLfvzbqztTlAgtPVZZvZfFyj1QVljylNvcdeQC6bPP-Lbl2DlqANaxsSODBeNbl43n0FCODflx7aLlHZvHpuR0MEOKZP6NYmPmP1mh-v7PE5lPtRGVVNMGcrIr59h_fJD2PHGqxH5WqBDQ",
                    "https://upload.wikimedia.org/wikipedia/commons/8/8f/North_Maluku_coa.png"
            },

            {
                    "21. Provinsi Nusa Tenggara Timur",
                    "14 Agustus 1958",
                    "Kupang",
                    "www.nttprov.go.id",
                    "Nusa Tenggara Timur " +
                            "adalah sebuah provinsi di Indonesia yang berada dalam gugusan Sunda Kecil dan termasuk dalam Kepulauan " +
                            "Nusa Tenggara. Provinsi yang biasa disingkat NTT ini memiliki 21 Kabupaten/Kota.Di awal kemerdekaan Indonesia, " +
                            "kepulauan ini merupakan wilayah Provinsi Sunda Kecil[5][6]. yang beribukota di kota Singaraja, kini terdiri atas" +
                            "3 provinsi (berturut-turut dari barat): Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur.  Setelah pemekaran, " +
                            "Nusa Tenggara Timur adalah sebuah provinsi Indonesia yang terletak di bagian tenggara Indonesia. Provinsi ini " +
                            "terdiri dari beberapa pulau, antara lain Pulau Flores, Pulau Sumba, Pulau Timor, Pulau Alor, Pulau Lembata, " +
                            "Pulau Rote, Pulau Sabu, Pulau Adonara, Pulau Solor, Pulau Komodo dan Pulau Palue. Ibukotanya terletak di Kupang, " +
                            "di bagian barat pulau Timor.",
                    "4.683.827 jiwa",
                    "47.676,51 km\u00b2",
                    "https://gdb.voanews.com/3B42AA84-12B3-4918-92F6-36FBF3A3FF85_w1023_r1_s.jpg",
                    "http://3.bp.blogspot.com/-5vYYFidLdgU/UNbOxCMm3RI/AAAAAAAAIlY/suBH242EKek/s1600/LOGO+PROVINSI+NTT.png"
            },

            {
                    "22. Provinsi Nusa Tenggara Barat",
                    "2 Juli 1958",
                    "Mataram",
                    "www.ntb.go.id",
                    "Nusa Tenggara Barat " +
                            "adalah sebuah provinsi di Indonesia yang berada dalam gugusan Sunda Kecil dan termasuk dalam Kepulauan Nusa " +
                            "Tenggara. Provinsi yang biasa disingkat NTB ini memiliki 10 Kabupaten/Kota. Di awal kemerdekaan Indonesia, " +
                            "wilayah ini termasuk dalam wilayah Provinsi Sunda Kecil yang beribukota di Singaraja. Kemudian, wilayah " +
                            "Provinsi Sunda Kecil dibagi menjadi 3 provinsi: Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur. Saat ini" +
                            "nama Nusa Tenggara digunakan oleh dua daerah administratif: Provinsi Nusa Tenggara Barat dan Nusa Tenggara" +
                            " Timur. Sesuai dengan namanya, provinsi ini meliputi bagian barat Kepulauan Nusa Tenggara. Dua pulau terbesar" +
                            " di provinsi ini adalah Lombok yang terletak di barat dan Sumbawa yang terletak di timur. Ibu kota provinsi" +
                            " ini adalah Kota Mataram yang berada di Pulau Lombok.",
                    "4.500.212 jiwa",
                    "19.950,22 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=88p1VBnoVaBByILsN3Btim3qacDtoigMDuwPGVvjwBxjdnnATrMvNlBm5nJu9YlvvJ6T-NeLjXbO4Qw6poiwqeM4NfkPWHhA8OarW047VDWQTTHIO-icsX9L9u1d4hyeMeHwKOJAkqd2k6gngRESfLLsCEEAgk57m7Z4o0v7t7jaPCBCrmZL1tVZ1ZvsuUhzIzJooBonpILYaBa5-ZiUUFAzMDKUSuBx7pdhr4_ry8gbmP53zvfhWzKvTf1bItgLEoINbiU2RNuvdUH4GYACBjbI6LCqG5SgLhjADknbR-XXt1Y",
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Coat_of_arms_of_West_Nusa_Tenggara.svg/1200px-Coat_of_arms_of_West_Nusa_Tenggara.svg.png"
            },

            {
                    "23. Provinsi Papua",
                    "10 September 1969",
                    "Jayapura",
                    " www.papua.go.id",
                    "Papua " +
                            "adalah sebuah provinsi terluas Indonesia yang terletak di bagian tengah Pulau Papua atau bagian paling timur " +
                            "wilayah Papua milik Indonesia. Belahan timurnya merupakan negara Papua Nugini. Provinsi Papua dulu mencakup " +
                            "seluruh wilayah Papua Bagian barat, namun sejak tahun 2003 dibagi menjadi dua provinsi dengan bagian timur tetap " +
                            "memakai nama Papua sedangkan bagian baratnya memakai nama Papua Barat. Papua memiliki luas 808.105 km persegi " +
                            "dan merupakan pulau terbesar kedua di dunia dan terbesar pertama di Indonesia.",
                    "2.831.381 jiwa",
                    "309.934,40 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=0mZLAn7yHHyz2WjYHkdIvwBJxy_PDfqj3oCNHPe3uE9RF_FDqSsgtbwAgqUqvEf_YVutGgJ5dX41INmKS6w5sVlj6BRMzUaiMmLWKl3S9bs4khUKW6g3DEh1pue9lJWQzCqTckRJR8f7f6TbS6ZqreeDRmJjWWdU6xspvbrRvn4w169NVkdl0dzufDuiKkBe6ZzH6RpK4fmRomVPrB5cJas82CMYAn0",
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Coat_of_arms_of_Papua.svg/1200px-Coat_of_arms_of_Papua.svg.png"
            },

            {
                    "24. Provinsi Papua Barat",
                    "4 Oktober 1999",
                    "Manokwari",
                    "www.papuabaratprov.go.id",
                    "Papua Barat " +
                            "adalah sebuah provinsi Indonesia yang terletak di ujung barat Pulau Papua. Ibukotanya adalah Manokwari. Nama " +
                            "provinsi ini sebelumnya adalah Irian Jaya Barat yang ditetapkan dalam Undang-Undang Nomor 45 Tahun 1999. " +
                            "Berdasarkan Peraturan Pemerintah Nomor 24 Tahun 2007 tanggal 18 April 2007, nama provinsi ini diubah menjadi " +
                            "Papua Barat. Papua Barat dan Papua merupakan provinsi yang memperoleh status otonomi khusus. Provinsi Papua " +
                            "Barat ini meski telah dijadikan provinsi tersendiri, namun tetap mendapat perlakuan khusus sebagaimana provinsi " +
                            "induknya. Provinsi ini juga telah mempunyai KPUD sendiri dan menyelenggarakan pemilu untuk pertama kalinya pada " +
                            "tanggal 5 April 2004.",

                    "760.855  jiwa",
                    "114.566,40 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=KnelK1TU0sB_7-7osLudNE0pZpo7ATat2re-7BGeKhUeAaXZe4Bcxexmw50L5X7zwTiZALCnDEQKyFxd1Etl9Av0p8MbOk-vUhGGC7uKKgN8nFMtn7vqdhWBi3gV625KOXbHsujFFH15vrpBSU6r9cWzPzB6rfcNdg7mOEIea0OV004_GtzT4chSoFSnCq93A3cwEEop8Pcb_sgkj20Q6s31RfcPintlw09lC2kOlyUNOOAcCAqBRzTILi1RAVCu1hzeMDCGAz-YKOlgXa8xbczQ8gRGg7CRcrdHBVZSv2sNMtE",
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Coat_of_arms_of_West_Papua.svg/1200px-Coat_of_arms_of_West_Papua.svg.png"
            },

            {
                    "25. Provinsi Sulawesi Barat",
                    "5 Oktober 2004",
                    "Mamuju",
                    "www.sulbar.go.id",
                    "Sulawesi Barat " +
                           "adalah provinsi hasil pemekaran dari provinsi Sulawesi Selatan. Pembentukan Provinsi Sulawesi Barat telah " +
                            "diperjuangkan sejak tahun 1960. Pada masa itu pulau Sulawesi terdapat 3 (tiga) Provinsi yakni Provinsi Sulawesi " +
                            "Selatan, Provinsi Sulawesi Tengah dan Provinsi Sulawesi Utara. Namun, pada tahun 1963 Pemekaran Provinsi di pulau " +
                            "Sulawesi oleh pemerintah pusat adalah pembentukan Provinsi Sulawesi Tenggara. Usulan pembentukan Provinsi " +
                            "Sulawesi Barat tidak disetujui Pemerintah Pusat. Perjuangan pembentukan Provinsi Sulawesi Barat kembali " +
                            "menemukan momentumnya pada tahun 1999 pasca gerakan reformasi. Pembentukan Provinsi Baru di Indonesia seperti " +
                            "Terbentuknya Provinsi Banten, Provinsi Bangka Belitung dan Provinsi Gorontalo menjadi semangat gerakan perjuangan" +
                            " pembentukan provinsi Sulawesi Barat. Perjuangan panjang pembentukan Provinsi Sulawesi Barat akhirnya terwujud " +
                            "melalui upaya massif rakyat Mandar dengan didukung oleh Anggota DPR RI melalui usulan Hak Inisiatif Anggota DPR " +
                            "RI tentang Undang-Undang Pembentukan Daerah Otonom Baru. Tanggal 5 Oktober 2004 Provinsi Sulawesi Barat Resmi " +
                            "terbentuk berdasarkan UU No. 26 Tahun 2004.",
                    "1.158.651 jiwa",
                    "16.787,19 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=w47WxUvFCbwyB_i8_ToO2SgHBKfyNQ2hUtSZFovCkhRq9SnHYD70w9Uzs1zRFXpw2tjD_PkaZQncrkduKGkAt8G6fbuxGLbjCjDUwrTuGiBnrBCEdpfYADuy4_G2_hK_qPe5oxosLwBxiN1npb1Gx7a9wZBpzs6gmP2od__keWU0XnzGoUnDb_sbtVy9_kUMR7_Xh011eTI4yv3kz1XJRNqG2hf3Frs",
                    "https://upload.wikimedia.org/wikipedia/commons/d/da/West_Sulawesi_coa.png"
            },

            {
                    "26. Provinsi Sulawesi Selatan",
                    "13 Desember 1960",
                    "Makassar",
                    "www.sulsel.go.id",
                    "Sulawesi Selatan " +
                            "adalah sebuah provinsi di Indonesia yang terletak di bagian selatan Sulawesi. Ibu kotanya adalah Makassar, dahulu" +
                            " disebut Ujung Pandang.Provinsi Sulawesi Selatan terletak di 0°12' - 8° Lintang Selatan dan 116°48' - 122°36' Bujur Timur. Luas " +
                            "wilayahnya 45.764,53 km². Provinsi ini berbatasan dengan Sulawesi Tengah dan Sulawesi Barat di utara, Teluk Bone" +
                            " dan Sulawesi Tenggara di timur, Selat Makassar di barat dan Laut Flores di selatan.",
                    "8.034.776 jiwa",
                    "46.116,45 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=-aca72-svSGQepg3eH865SCtCqUn5b1W42LgP-_lrr0-tXxpdNOdbL-68ECQAqOfqH-fK6D_jtJHNyCAZiONaZG3a02rnl-c0S4OmFAStNpEqWlE4wjAYcvf0m55U5FSqY8XgHoG4R9N5gTnFvbY0FTti44R4bit3Y5oa6_2Uqa1Zk-607OKM-0pXvlIJYEwRiylnIDNjBALN5m-ZmNejQhVnv_qdxPpRrms_hfk7qLTP2YlFVg2KoEiIoco_ISdllAsASXM9icrCVtC0Eai_J3M1fe9ct59YU6KKaUdrXSUQA",
                    "https://3.bp.blogspot.com/-gN11Bm9bZwE/Vx4sIt0hy7I/AAAAAAAAPRo/X2c-dNo52Q8QLIWNjhj5LQwfot0KVTLqgCLcB/s1600/sulawesi%2Bselatan.png"
            },

            {
                    "27. Provinsi Sulawesi Tengah",
                    "23 September 1964",
                    "Palu",
                    " www.sulteng.go.id",
                    "Sulawesi Tengah " +
                            "Sulawesi Tengah adalah sebuah provinsi di bagian tengah Pulau Sulawesi, Indonesia. Ibu kota provinsi ini adalah " +
                            "Kota Palu. Luas wilayahnya 61.841,29 km², dan jumlah penduduknya 3.222.241 jiwa (2015). Sulawesi Tengah memiliki " +
                            "wilayah terluas di antara semua provinsi di Pulau Sulawesi, dan memiliki jumlah penduduk terbanyak kedua di Pulau " +
                            "Sulawesi setelah provinsi Sulawesi Selatan. Gubernur yang menjabat sekarang adalah Drs. H. Longki Djanggola, " +
                            "M.Si. bersama dengan (Alm) Sudarto untuk periode kedua.",
                    "2.294.841 jiwa",
                    "68.089,83 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=BnRwYnncRVeIEe42ufVWs5n1uEYYNRrGrxw59HIMvUE9S0-BtJ3bBPsLBG0dFKhdI97FAlyhhVdD2jqsPMPpOhRiHVvl0X4trClBXmKnReCnkFX7M9_ywi8bExmF_i1nLr_aD92z3uZd8ziu4R9W23sb4PQVIcBEaPIdaGoBUF6K57xMIKEZzop-Ty2R5vim3QByY3VlyQg6OByXei_g-6bAsp2QVL2lkVRorGGuiMQYYHPzXLKuTftERhKPi-w9EfrR9EmtQO3ryDrH6kmVPtvIcdU73tJycttAP-QdNS4KlcA",
                    "https://upload.wikimedia.org/wikipedia/commons/e/e1/Central_Sulawesi_coa.png"
            },

            {
                    "28. Provinsi Sulawesi Tenggara",
                    "23 September 1964",
                    "Kendari",
                    "www.sultra.go.id",
                    "Sulawesi Tenggara " +
                            "Sulawesi Tenggara merupakan sebuah provinsi di Indonesia yang terletak bagian tenggara pulau Sulawesi dengan ibukota Kendari." +
                            "Provinsi Sulawesi Tenggara terletak di Jazirah Tenggara Pulau Sulawesi, secara geografis terletak di bagian selatan garis " +
                            "khatulistiwa di antara 02°45' – 06°15' Lintang Selatan dan 120°45' – 124°30' Bujur Timur serta mempunyai wilayah daratan seluas " +
                            "38.140 km² (3.814.000 ha) dan perairan (laut) seluas 110.000 km² (11.000.000 ha).",
                    "2.232.586  jiwa",
                    "36.757,45 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=tRT5WsSMXfO8yGtskrQ5ayPShHfTSwzDaenkllv1QdCQwmdOP4nQ5cId2vFpi-_DkhWCTURmxTv_5Ro8fegbWzkyMV1aBunvSAXhrEBLg1GGFITDmo4NrSIVTsj7u_QlVuQZXOmfm3WpSVGYm3fAa6a_uWf6F0eA27qRmS596PgzmnABoW2vX3CEuss6c4nYEpHkAAiYu32DeT-Y_vKhRAEe2hOqEwEjaTdgD89R8vDjUYQm5DnoDs-ANzfpMyi1tO1F1hFArYs9UOzi9e15NtsRDx2-jg7gbReNsK-hGUHWif8",
                    "https://ardilamadi2.files.wordpress.com/2012/08/5a9a8-logoprovinsisulawesitenggara.png?w=1400"
            },

            {
                    "29. Provinsi Sulawesi Utara",
                    "29 November 1956",
                    "Manado",
                    "www.sulut.go.id",
                    "Sulawesi Utara " +
                            "adalah salah satu provinsi yang terletak di ujung utara Pulau Sulawesi dengan ibu kota terletak di kota Manado. " +
                            "Sulawesi Utara atau Sulut berbatasan dengan Laut Maluku dan Samudera Pasifik di sebelah timur, Laut Maluku dan " +
                            "Teluk Tomini di sebelah selatan, Laut Sulawesi dan provinsi Gorontalo di sebelah barat, dan provinsi Davao del " +
                            "Sur (Filipina) di sebelah utara.",
                    "2.270.596  jiwa",
                    "13.930,73 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=LqNsT3iCDa8XeNlHMDX5c8hv0dFyKfogPSWzfHg545Y2pq2xyWIjehs1e6qv-nhuneBa3DdASVocPZSu6u-AOWJ3OhYElVWLCE4FBOosKKviXr1SjDacQVW1h4gdvb_rQb46ox9JT2KIrUqrqgL1fpIc27ALUaekZU9qss4cqpP2DjZnlFofFpH6kEVugwjY_tFjWC3r7KGakB_X2aIaS0KIk5aiUXHxCCrYXGqSxVos95waTW1LiALhjbXhPaKU9TCiPFry29SE3i8hbblMowYJhyT28aCQ1qvnlcLRb-PUpMU",
                    "http://www.sulutprov.go.id/contents/North_Sulawesi_Emblem.jpg"
            },

            {
                    "30. Sumatera Barat",
                    "4 Oktober 1999",
                    "Padang",
                    "www.sumbarprov.go.id",
                    "Sumatera Barat " +
                            "adalah salah satu provinsi di Indonesia yang terletak di pulau Sumatera dengan Padang sebagai ibu kotanya. " +
                            "Sesuai dengan namanya, wilayah provinsi ini menempati sepanjang pesisir barat Sumatera bagian tengah, dataran " +
                            "tinggi Bukit Barisan di sebelah timur, dan sejumlah pulau di lepas pantainya seperti Kepulauan Mentawai. Dari " +
                            "utara ke selatan, provinsi dengan wilayah seluas 42.297,30 km² ini berbatasan dengan empat provinsi, yakni " +
                            "Sumatera Utara, Riau, Jambi, dan Bengkulu. Sumatera Barat adalah rumah bagi etnis Minangkabau, walaupun wilayah " +
                            "adat Minangkabau sendiri lebih luas dari wilayah administratif Provinsi Sumatera Barat saat ini. Provinsi ini " +
                            "berpenduduk sebanyak 4.846.909 jiwa dengan mayoritas beragama Islam. Provinsi ini terdiri dari 12 kabupaten dan " +
                            "7 kota dengan pembagian wilayah administratif sesudah kecamatan di seluruh kabupaten (kecuali kabupaten Kepulauan" +
                            "Mentawai) dinamakan sebagai nagari.",
                    "4.846.909  jiwa",
                    "42.297,30 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=MGtLXuSh77hDDayb-YGB09BbJzZFTihEPeJeuRCIi5fGMraTLLAeQ_GhzA6qUKiiWQ-loXNuEAaYiK22lfRdccz4VCpxGuKih-uryurvBtJym8FkeU3YaI2yIDAoEGSdyv_3sXPZUhaoE0TDP6UUlKvyXGSOKhV5C8CY3sGnuS0-QhWpOM7-LrBU9CLtlu_e__ZLriRYfClSO0u94_3PlNJURMRFg1Fuk9Ll9vGlkwwVtPQVMqXqjREVc2cEr3kWnVnzaeY92YKnvdiytMqcjDBmw9VcxTjEqKITyY1usQGYbYs",
                    "http://1.bp.blogspot.com/-LXCUNQWJTP0/UJo_RK5c9UI/AAAAAAAAAR8/bPWz4Qsj4l4/s1600/cdr-logo-sumatera-barat.jpg"
            },

            {
                    "31. Sumatera Selatan",
                    "4 Oktober 1999",
                    "Palembang",
                    "www.sumselprov.go.id",
                    "Sumatera Selatan " +
                            "Sumatera Selatan adalah salah satu provinsi di Indonesia yang terletak di bagian selatan Pulau Sumatera. " +
                            "Provinsi ini beribukota di Palembang. Secara geografis provinsi Sumatera Selatan berbatasan dengan provinsi " +
                            "Jambi di utara, provinsi Kep. Bangka-Belitung di timur, provinsi Lampung di selatan dan Provinsi Bengkulu di " +
                            "barat. Provinsi ini kaya akan sumber daya alam, seperti minyak bumi, gas alam dan batu bara. Selain itu ibu kota " +
                            "provinsi Sumatera Selatan, Palembang, telah terkenal sejak dahulu karena menjadi pusat Kerajaan Sriwijaya. Di " +
                            "samping itu, provinsi ini banyak memiliki tujuan wisata yang menarik untuk dikunjungi seperti Sungai Musi, " +
                            "Jembatan Ampera, Pulau Kemaro, Danau Ranau, Kota Pagaralam dan lain-lain. Karena sejak dahulu telah menjadi pusat " +
                            "perdagangan, secara tidak langsung ikut memengaruhi kebudayaan masyarakatnya. Makanan khas dari provinsi ini " +
                            "sangat beragam seperti pempek, model, tekwan, pindang patin, pindang tulang, sambal jokjok, berengkes dan tempoyak.",
                    "7.450.394  jiwa",
                    "85.679,42 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=QfN8bHHK3r4cPTEqArUFfLTEDURSfml1-TVrCpaCseaRyby4hzr6EsJINb5T9n8vE53Jy5bPGm6lt0PpqxmWSu1cBsVT4wuc4h3Z5YVElQR2ejbmzJ1vSoCNGfhOVBCNoHhQhCtlDCvwsC7g-wk9-03gzLMnjpdOWP3R1byB11WZXx1UK03krP6npf3loJl4dmFAMUtBwxMlKzGce8WCwM1DOBmel2ZwHIfVytRbrNj6DKH8pUJi5q5mAR6NO8AJfj_MviPWZNW8HMkB_tzasyLVqvewBEXco4qpWUcfVORYcVs",
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Coat_of_arms_of_South_Sumatra.svg/1200px-Coat_of_arms_of_South_Sumatra.svg.png"
            },

            {
                    "32. Sumatera Utara",
                    "29 November 1956",
                    "Medan",
                    "www.sumutprov.go.id",
                    "Sumatera Utara " +
                            "adalah sebuah provinsi yang terletak di Pulau Sumatera, Indonesia dan beribukota di Medan. Dengan " +
                            "diterbitkannya Undang-Undang Republik Indonesia (R.I.) No. 10 Tahun 1948 pada tanggal 15 April 1948," +
                            "ditetapkan bahwa Sumatera dibagi menjadi tiga provinsi yang masing-masing berhak mengatur dan mengurus " +
                            "rumah tangganya sendiri yaitu: Provinsi Sumatera Utara, Provinsi Sumatera Tengah, dan Provinsi Sumatera Selatan." +
                            "Tanggal 15 April 1948 selanjutnya ditetapkan sebagai hari jadi Provinsi Sumatera Utara.",
                    "12.982.204  jiwa",
                    "72.981,23 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=QUiaXH3kcPor49K-3k-Q6DE-TNBIGC-gktcoyOYNzJYVmLNaYsG_xrCx1P3VJCMIKqnWLTXqLA3OCKvcHV14Nq6iLUfDMZp56KajJHlwBUQj6NRRwJEn-GKZvPj3khgdvBv1KTDUlJO4VTWjMf2luYbVumqyGW4IK04TcL03s51TPujrKpiRLHs9xqRpxj1Gpz15ysoF5otGRFW7evwXiKasHFPbUqloYeRtsSDR0Zn4oUhf9_lOafcwnK4eTbqujZ_W0_kpup14XF9VK2vM6wSvJbGypT1rLliHkHGpTXVVS8U",
                    "http://1.bp.blogspot.com/-fcEHnGPGLxY/T4LzmVAfA9I/AAAAAAAAFkc/rGPa1UQpM2w/s1600/LOGO+PROVINSI+SUMATERA+UTARA.png"
            },

            {
                    "33. Daerah Istimewa Yogyakarta",
                    "4 Maret 1950",
                    "Yogyakarta",
                    "www.jogjaprov.go.id",
                    "Papua Barat " +
                            "adalah sebuah provinsi Indonesia yang terletak di ujung barat Pulau Papua. Ibukotanya adalah Manokwari. Nama " +
                            "provinsi ini sebelumnya adalah Irian Jaya Barat yang ditetapkan dalam Undang-Undang Nomor 45 Tahun 1999. " +
                            "Berdasarkan Peraturan Pemerintah Nomor 24 Tahun 2007 tanggal 18 April 2007, nama provinsi ini diubah menjadi " +
                            "Papua Barat. Papua Barat dan Papua merupakan provinsi yang memperoleh status otonomi khusus. Provinsi Papua " +
                            "Barat ini meski telah dijadikan provinsi tersendiri, namun tetap mendapat perlakuan khusus sebagaimana provinsi " +
                            "induknya. Provinsi ini juga telah mempunyai KPUD sendiri dan menyelenggarakan pemilu untuk pertama kalinya pada " +
                            "tanggal 5 April 2004.",

                    "3.343.651 jiwa",
                    "3.133,15 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=5AHwrBOhts-3neUrqvWa7CPWZEAaNp4JGQJUq0KpdGWR2GU4DfoNiUhhMt49_ni19kRyYDQnpFeZubVsVm-q-vIP5qpeFhL4Nyv-JU7alX2-pHsiG6hFFRnx9zPV6AikVLa5yFPyhIcpiXydC5OTchpkeBpii-TWZ5_HLD2bUoU5Si8KbdJFkopWuOizXXF_N7EP-XlzCrfkRsDBAqEzS-X0ZO5jbJGS5WXuxiWkudVoGEZ7Vhg3dQYMKfSvhwvzN3sb96ZfHGh4J78l9SnNxF8rWnmxAor3rA7APenx13CSqIk",
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Coat_of_arms_of_Yogyakarta.svg/180px-Coat_of_arms_of_Yogyakarta.svg.png"
            },

            {
                    "34. Provinsi Jambi",
                    "25 Juni 1958",
                    "Jambi",
                    "www.jambiprov.go.id",
                    "Jambi " +
                            "adalah sebuah Provinsi Indonesia yang terletak di pesisir timur di bagian tengah Pulau Sumatera. " +
                            "Jambi adalah satu dari tiga provinsi di Indonesia yang ibukotanya bernama sama dengan nama provinsinya, " +
                            "selain Bengkulu dan Gorontalo.Provinsi Jambi secara geografis terletak antara 0,45° Lintang Utara, 2,45° " +
                            "Lintang Selatan dan antara 101,10°-104,55° Bujur Timur. Di sebelah Utara berbatasan dengan Provinsi Riau, " +
                            "sebelah Timur dengan Selat Berhala, sebelah Selatan berbatasan dengan Provinsi Sumatera Selatan dan sebelah " +
                            "Barat dengan Provinsi Sumatera Barat dan Provinsi Bengkulu. Kondisi geografis yang cukup strategis di antara " +
                            "kota-kota lain di provinsi sekitarnya membuat peran provinsi ini cukup penting terlebih lagi dengan dukungan" +
                            "sumber daya alam yang melimpah. Kebutuhan industri dan masyarakat di kota-kota sekelilingnya didukung suplai " +
                            "bahan baku dan bahan kebutuhan dari provinsi ini.",

                    "2.683.289  jiwa",
                    "53.010,22 km\u00b2",
                    "https://www.google.co.id/maps/vt/data=OJVvZ3aC50-vOMILM6Vze5C05oaC7TvqWa_XbopMqnrDO3_TOkWhFofz45DAcnIglhcfTsr2_j5PQ9YQx5VpAcsse_VNNHAgMIRJxCprciO5Ruh68i_6sIWISjd4-x5l5D-uB5oGwf13Hw1CoejpWISiIz4aADLo2-voHhflSwGVgSCS3uKDgmnUzG7sDHwu4OIA3gj1qyZaRKY0yPKEEF35ykWnxKljq-kdF2XTAu214UXwk9syKHL92J0jqxhbYCbyImTct4AIwXH6q4oJaMR69CqFP2bbnVM8HU9XBz64aZc",
                    "https://2.bp.blogspot.com/-H-uCx7nuUhU/VPuwwIrhluI/AAAAAAAABug/Q7OWJYYwxRE/s1600/Logo+Provinsi+Jambi.png"
            },

    };

    public static ArrayList<Provinsi> getListData(){
        Provinsi provinsi = null;
        ArrayList<Provinsi> list = new ArrayList<>();
        for (int i = 0; i <data.length; i++) {
            provinsi = new Provinsi();
            provinsi.setNama(data[i][0]);
            provinsi.setTgl_berdiri(data[i][1]);
            provinsi.setIbu_kota(data[i][2]);
            provinsi.setSitus_web(data[i][3]);
            provinsi.setSejarah(data[i][4]);
            provinsi.setKepadatan(data[i][5]);
            provinsi.setLuas(data[i][6]);
            provinsi.setFoto(data[i][7]);
            provinsi.setLogo(data[i][8]);
            list.add(provinsi);
        }

        return list;
    }


}
