package com.app.smktelkom.indonesia_province;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by SMK TELKOM on 06/01/2018.
 */

public class CardViewProvinsiAdapter extends RecyclerView.Adapter<CardViewProvinsiAdapter.CardViewViewHolder> {
    private ArrayList<Provinsi> listProvinsi;
    private Context context;
    private View itemView;

    public CardViewProvinsiAdapter(View itemView, ArrayList<Provinsi> listProvinsi,Context context)
    {
        this.context = context;
        this.listProvinsi = listProvinsi;
        this.itemView = itemView;
    }

    public CardViewProvinsiAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<Provinsi> getListProvinsi() {
        return listProvinsi;
    }

    public void setListProvinsi(ArrayList<Provinsi> listProvinsi) {
        this.listProvinsi = listProvinsi;
    }
    @Override
    public CardViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_provinsi, parent, false);
        CardViewViewHolder viewHolder = new CardViewViewHolder(view, context, listProvinsi);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CardViewViewHolder holder, int position) {

        Provinsi p = getListProvinsi().get(position);

        Glide.with(context)
                .load(getListProvinsi().get(position).getFoto())
                .fitCenter()
                .crossFade()
                .into(holder.imgFoto);

        holder.tvNama.setText(p.getNama());
        holder.tvIbuKota.setText(p.getIbu_kota());
        holder.tvSitus.setText(p.getSitus_web());
    }

    @Override
    public int getItemCount() {
        return getListProvinsi().size();
    }

    public class CardViewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgFoto, imgLogo;
        TextView tvNama, tvIbuKota, tvSitus, tvTglBerdiri, tvKepadatan, tvLuas;
        Button btnSelengkapnya;
        ArrayList<Provinsi> listProvinsi = new ArrayList<Provinsi>();
        Context context;


        public CardViewViewHolder(View itemView, Context context, ArrayList<Provinsi> listProvinsi) {
            super(itemView);
            this.listProvinsi = listProvinsi;
            this.context = context;
            imgFoto = (ImageView) itemView.findViewById(R.id.img_item_foto);
            imgLogo = (ImageView) itemView.findViewById(R.id.img_item_logo);
            tvNama = (TextView) itemView.findViewById(R.id.tv_item_nama);
            tvIbuKota = (TextView) itemView.findViewById(R.id.tv_ibu_kota);
            tvSitus = (TextView) itemView.findViewById(R.id.tv_situs_web);
            tvTglBerdiri = (TextView) itemView.findViewById(R.id.tv_tanggal_berdiri);
            tvKepadatan = (TextView) itemView.findViewById(R.id.tv_kepadatan);
            tvLuas = (TextView) itemView.findViewById(R.id.tv_luas_wilayah) ;
            btnSelengkapnya = (Button) itemView.findViewById(R.id.btn_selengkapnya);
            btnSelengkapnya.setOnClickListener(this);
        }

        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_selengkapnya:
                    int position = getAdapterPosition();
                    Provinsi listProvinsi = this.listProvinsi.get(position);
                    Intent intent = new Intent(context, Selengkapnya.class);
                    intent.putExtra("tv_nama", listProvinsi.getNama());
                    intent.putExtra("tv_tgl", listProvinsi.getTgl_berdiri());
                    intent.putExtra("tv_ibuKota", listProvinsi.getIbu_kota());
                    intent.putExtra("tv_situs", listProvinsi.getSitus_web());
                    intent.putExtra("tv_sejarah", listProvinsi.getSejarah());
                    intent.putExtra("tv_kepadatan", listProvinsi.getKepadatan());
                    intent.putExtra("tv_luas", listProvinsi.getLuas());
                    intent.putExtra("tv_logo", listProvinsi.getLogo());
                    this.context.startActivity(intent);
            }
        }
    }
}


