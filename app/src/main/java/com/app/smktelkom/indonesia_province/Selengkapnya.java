package com.app.smktelkom.indonesia_province;

import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class Selengkapnya extends AppCompatActivity {

    ImageView tv_logo;
    TextView tv_nama, tv_tgl, tv_ibuKota, tv_situs, tv_sejarah, tv_kepadatan, tv_luas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setTitle("" +getIntent().getStringExtra("tv_nama"));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selengkapnya);
        tv_logo = (ImageView) findViewById(R.id.img_item_logo);
        tv_nama = (TextView) findViewById(R.id.tv_item_nama);
        tv_tgl = (TextView) findViewById(R.id.tv_tanggal_berdiri);
        tv_ibuKota = (TextView) findViewById(R.id.tv_ibu_kota);
        tv_situs = (TextView) findViewById(R.id.tv_situs_web);
        tv_sejarah = (TextView) findViewById(R.id.tv_sejarah_provinsi);
        tv_kepadatan = (TextView) findViewById(R.id.tv_kepadatan);
        tv_luas = (TextView) findViewById(R.id.tv_luas_wilayah);

        tv_logo.setImageResource(getIntent().getIntExtra("tv_logo", 00));
        tv_tgl.setText("" +getIntent().getStringExtra("tv_tgl"));
        tv_ibuKota.setText("" +getIntent().getStringExtra("tv_ibuKota"));
        tv_situs.setText("" +getIntent().getStringExtra("tv_situs"));
        tv_sejarah.setText("" +getIntent().getStringExtra("tv_sejarah"));
        tv_kepadatan.setText("" + getIntent().getStringExtra("tv_kepadatan"));
        tv_luas.setText(""+getIntent().getStringExtra("tv_luas"));

        Glide.with(this)
                .load(getIntent().getStringExtra("tv_logo"))
                .fitCenter()
                .crossFade()
                .into(tv_logo);

    }

}
