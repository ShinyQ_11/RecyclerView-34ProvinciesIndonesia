package com.app.smktelkom.indonesia_province;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public RecyclerView rvCategory;
    public ArrayList<Provinsi>list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvCategory = (RecyclerView)findViewById(R.id.rv_category);
        rvCategory.setHasFixedSize(true);

        list = new ArrayList<>();
        list.addAll(DataProvinsi.getListData());

        showRecyclerCardView();
        getSupportActionBar().setTitle("34 Provinsi Indonesia");
    }


    private void showRecyclerCardView(){
        rvCategory.setLayoutManager(new LinearLayoutManager(this));
        CardViewProvinsiAdapter cardViewPresidentAdapter = new CardViewProvinsiAdapter(this);
        cardViewPresidentAdapter.setListProvinsi(list);
        rvCategory.setAdapter(cardViewPresidentAdapter);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.main_menu:
                getSupportActionBar().setTitle("34 Provinsi Indonesia");
                showRecyclerCardView();
                break;

            case R.id.about:
                startActivity(new Intent(MainActivity.this , About_app.class));
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
